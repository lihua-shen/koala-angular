import {Component, OnInit, OnChanges} from '@angular/core';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import {debug, log} from 'util';
import {logger} from 'codelyzer/util/logger';
import {Http, RequestOptions, Response} from '@angular/http';
import {HttpClient, HttpErrorResponse, HttpHeaders, HttpParams} from '@angular/common/http';
import {URLSearchParams} from '@angular/http';
import {isUndefined} from 'util';
import {forEach} from '@angular/router/src/utils/collection';
import {SearchService} from '../../core/search/search.service';

@Component({
  selector: 'app-trading',
  templateUrl: './trading.component.html',
  styleUrls: ['./trading.component.scss']
})
export class TradingComponent implements OnInit {

  tickerControl = new FormControl();
  strategyControl = new FormControl();
  tickers: string[] = ['AAPL', 'BG', 'GOOG', 'REMX', 'RIO',
    'HON', 'MSFT', 'NUAN', 'OLN', 'SIEGY',
    'MRK',
    'AA',
    'BOM'];
  strategies: string[] = ['None', 'Moving Average', 'Bollinger Bands'];
  filteredTickerOptions: Observable<string[]>;
  filteredStrategyOptions: Observable<string[]>;
  transactions: string[] = [];
  pl = '0';

  constructor(private http: HttpClient) {
  }

  ngOnInit() {
    this.http.get('http://localhost:3000/transactions?period=10')
      .subscribe(
        data => {
          console.log(data);
          // const rows = JSON.parse(data);
          console.log(data[0].action);
          this.transactions = [];
          for (var i in data) {
            var row = data[i];
            this.transactions.push('id: ' + row.id + ' action: ' + row.action + ' price: ' + row.price + ' size: ' + row.size + ' ticker: ' + row.ticker + ' time: ' + row.time + '   \n   ');
          }
          // data.toString();


        });
    this.filteredTickerOptions = this.tickerControl.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(this.tickers, value))
    );

    this.filteredStrategyOptions = this.strategyControl.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(this.strategies, value))
    );

    this.http.get('http://localhost:3000/pl').subscribe(
      data => {
        console.log(data);
        this.pl = data;
        // const rows = JSON.parse(data);


      });
  }

  getPL() {
    this.http.get('http://localhost:3000/pl').subscribe(
      data => {
        console.log(data);
        this.pl = data;
        // const rows = JSON.parse(data);


      });
  }

  start() {
    this.http.get('http://localhost:3000/strat?ticker=AAPL&longTerm=10&shortTerm=5&strategy=MACD&threshold=3').subscribe(
      data => {
        console.log(data);
        // const rows = JSON.parse(data);


      });

  }

  private _filter(options: string[], value: string): string[] {
    const filterValue = value.toLowerCase();
    return options.filter(option => option.toLowerCase().startsWith(filterValue));
  }

}
